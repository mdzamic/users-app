const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

 // Create connection
 const db = mysql.createConnection({
     host     : 'localhost',
     user     : 'root',
     password : 'password',
     database : 'Users'
 });

 // Connect
db.connect((err) => {
    if(err){
        throw err;
    }
    console.log('MySql Connected...');
});

// Login user 
app.get('/authenticate', (req, res) => {
    let answer = { message: ''}
    let sql = `SELECT * FROM users WHERE email = '${req.query.email}'`;
    let query = db.query(sql, (err, result) => {
        if(err) {
            throw err
        } else {
            if (result.length > 0) {
                if (result[0].password === req.query.password) {
                    res.send(result)
                } else {
                    answer.message = 'Email and Password do not match!'
                    res.send(JSON.stringify(answer))
                }
            } else {
                answer.message = 'Email does not exists!'
                res.send(JSON.stringify(answer))
            }
        }      
    })
});

// Add user
app.get('/add', (req, res) => {
    let user = req.query;
    let sql = 'INSERT INTO users SET ?';
    let query = db.query(sql, user, (err, result) => {
        if(err) throw err;
        res.send(`${result.insertId}`);
    });
}); 

// Remove user
app.get('/deleteuser/:id', (req, res) => {
    let sql = `DELETE FROM users WHERE id = ${req.params.id}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        res.send('User deleted...');
    });
});

// Edit user
app.get('/updateuser/:id', (req, res) => {
    let userUpdate = req.query;
    let sql = `UPDATE users SET firstname='${userUpdate.firstname}', lastname='${userUpdate.lastname}', email='${userUpdate.email}', password='${userUpdate.password}' WHERE id = ${req.params.id}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        res.send('User updated...');
    });
});

//Select single user
app.get('/getuser/:id', (req, res) => {
    let answer = { message: ''}
    let sql = `SELECT * FROM users WHERE id = ${req.params.id}`;
    let query = db.query(sql, (err, result) => {
        if(err) {
            throw err
        } else {
            if (result.length > 0) {
                res.send(result)
            } else {
                answer.message = 'User does not exists!'
                res.send(JSON.stringify(answer))
            }
        }
    });
});

// Get all users
app.get('/getusers', (req, res) => {
    let sql = 'SELECT * FROM users';
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        res.send(result);
    });
});


app.listen('4000', () => {
    console.log('Server started on port 4000');
});