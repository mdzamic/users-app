import { userConstants } from '../_constants/user.constants';
import { alertActions } from './alert.actions';
import { history } from '../_helpers/history';

// AUTH_USER
export const request = (user) => ({ 
    type: userConstants.LOGIN_REQUEST, 
    user 
});

export const success = (user) => ({ 
    type: userConstants.LOGIN_SUCCESS, 
    user 
});

export const failure = (error) => ({ 
    type: userConstants.LOGIN_FAILURE, 
    error 
});

export const login = (email, password) => {
    return (dispatch) => {
        dispatch(request({ email }))
        return fetch(`http://localhost:4000/authenticate?email=${email}&password=${password}`)
        .then(response => response.json())
        .then(response =>  {
            if (Array.isArray(response)) {
                localStorage.setItem('user', JSON.stringify(response));
                dispatch(success(response));
                history.push('/');
            } else {
                dispatch(failure(response.message));
                dispatch(alertActions.error(response.message));
            }
        });
    };
};

export const logout = () => {
    localStorage.removeItem('user');
    return { 
        type: userConstants.LOGOUT 
    };
};


// ADD_USER
export const addUser = (user) => ({
    type: userConstants.ADD_USER,
    user
});

export const startAddUser = (userData = {}) => {
    return (dispatch) => {
        const {
            firstname = '',
            lastname = '',
            email = '',
            password = ''
        } = userData;
        const user = { firstname, lastname, email, password };
        return fetch(`http://localhost:4000/add?firstname=${firstname}&lastname=${lastname}&email=${email}&password=${password}`)
            .then((response) => response.json())
            .then(({ data }) => {
                dispatch(addUser({
                    id: data,
                    ...user
            }));
        });
    };
};

// REMOVE_USER
export const removeUser = ({ id } = {}) => ({
    type: userConstants.REMOVE_USER,
    id
});

export const startRemoveUser = ({ id } = {}) => {
    return (dispatch) => {
        return fetch(`http://localhost:4000/deleteuser/${id}`)
        .then(() => {
            dispatch(removeUser({ id }));
        });
    };
};

// EDIT_USER
export const editUser = (id, updates) => ({
    type: userConstants.EDIT_USER,
    id,
    updates
});

export const startEditUser = (id, updates) => {
    return (dispatch) => {
        const {
            firstname,
            lastname,
            email,
            password
        } = updates;
        return fetch(`http://localhost:4000/updateuser/${id}?firstname=${firstname}&lastname=${lastname}&email=${email}&password=${password}`)
        .then(() => {
            dispatch(editUser(id, updates));
        });
    };
};

// GET_SINGLE_USER

export const getSingleUser = (user) => ({
    type: userConstants.GET_SINGLE_USER,
    user
});

export const startGetSingleUser = ({ id } = {}) => {
    return (dispatch) => {
        return fetch(`http://localhost:4000/getuser/${id}`)
        .then(response => response.json())
        .then(response =>  {
            if (Array.isArray(response)) {
                const user = response[0];
                dispatch(getSingleUser(user));    
            } else {
                dispatch(alertActions.error(response.message));
            }
            
        });
    };
};

// GET_ALL_USERS
export const setUsers = (users) => ({
    type: userConstants.GET_ALL_USERS,
    users
});

export const startSetUsers = () => {
    return (dispatch) => {
        return fetch(`http://localhost:4000/getusers`)
        .then(response => response.json())
        .then(response =>  {
            const users = []
            response.forEach((res) => {
                users.push({
                    id: res.id,
                    ...res,            
                });
            });
            dispatch(setUsers(users));
        });
    };
};
