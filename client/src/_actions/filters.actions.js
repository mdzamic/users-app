import { filterConstants } from '../_constants/filter.constants';

export const setTextFilter = (text='') => ({
    type: filterConstants.SET_TEXT_FILTER,
    text
});

export const sortByFirstName = () => ({
    type: filterConstants.SORT_BY_FIRSTNAME,
});

export const sortByLastName = () => ({
    type: filterConstants.SORT_BY_LASTNAME
});

export const sortByEmail = () => ({
    type: filterConstants.SORT_BY_EMAIL,
});

export const sortById = () => ({
    type: filterConstants.SORT_BY_ID,
});

