// Get visible Users
export default (users, {text, sortBy}) => {
    return users.filter((user) => {
        if (sortBy === 'firstname') {
            return user.firstname.toLowerCase().includes(text.toLowerCase());
        } else if (sortBy === 'lastname') {
            return user.lastname.toLowerCase().includes(text.toLowerCase());
        } else if (sortBy === 'email') {
            return user.email.toLowerCase().includes(text.toLowerCase());
        } else {
            if (isNaN(parseInt(text))) {
                return true
            } else {
                return user.id === parseInt(text)
            } 
        }
    }).sort((a,b) => {
        if (sortBy === 'firstname') {
            let n = a.firstname.toLowerCase().localeCompare(b.firstname.toLowerCase())
            return (n === 1) ? 1 : -1
        } else if (sortBy === 'lastname') {
            let n = a.lastname.toLowerCase().localeCompare(b.lastname.toLowerCase())
            return (n === 1) ? 1 : -1
        } else if (sortBy === 'email'){
            let n = a.email.toLowerCase().localeCompare(b.email.toLowerCase())
            return (n === 1) ? 1 : -1
        } else {
            return a.id > b.id ? 1 : -1
        }
    });
};