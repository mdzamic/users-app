import React, { Fragment }from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Header } from '../_components/Header';
import { Footer } from '../_components/Footer';


export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        localStorage.getItem('user')
            ?   <Fragment>
                    <Header />
                    <Component {...props} />
                    <Footer />
                </Fragment>
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    )} />
)