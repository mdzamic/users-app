export const userConstants = {
    LOGIN_REQUEST: 'USERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'USERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'USERS_LOGIN_FAILURE',
    
    LOGOUT: 'USERS_LOGOUT',

    ADD_USER: 'ADD_USER',
    REMOVE_USER: 'REMOVE_USER',
    EDIT_USER: 'EDIT_USER',
    GET_SINGLE_USER: 'GET_SINGLE_USER',
    GET_ALL_USERS: 'GET_ALL_USERS'
};
