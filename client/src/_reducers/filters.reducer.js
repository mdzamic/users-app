// Filters Reducer
import { filterConstants } from '../_constants/filter.constants';

const filtersReducerDefaultState = {
    text: '',
    sortBy: 'firstname',
};

export const filters = (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case filterConstants.SET_TEXT_FILTER:
            return {...state, text: action.text};
        case filterConstants.SORT_BY_FIRSTNAME:
            return {...state, sortBy: 'firstname'};
        case filterConstants.SORT_BY_LASTNAME:
            return {...state, sortBy: 'lastname'};
        case filterConstants.SORT_BY_EMAIL:
            return {...state, sortBy: 'email'};
        case filterConstants.SORT_BY_ID:
            return {...state, sortBy: 'id'};
        default:
            return state;
    }
};