import { userConstants } from '../_constants/user.constants';

// Users Reducer
const usersReducerDefaultState = [];

export const users = (state = usersReducerDefaultState, action) => {
    switch (action.type) {

        case userConstants.ADD_USER:
            return [...state, action.user];

        case userConstants.REMOVE_USER:
            return state.filter((user) => user.id !== action.id);

        case userConstants.EDIT_USER:
            return state.map((user) => {
                if (user.id === action.id) {
                return {
                    ...user, ...action.updates
                };
                } else {
                return user;
                }
            });
        
        case userConstants.GET_SINGLE_USER:
            return [action.user];

        case userConstants.GET_ALL_USERS:
            return action.users;
        
        default: 
            return state;
    }
};