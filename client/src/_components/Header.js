import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Header extends React.Component {
    
    render() {
        const { user } = this.props;
        return (
            <header className="header">
                <div className="content-container">
                    <div className="header__title header__content">
                        <Link className="header__title" to="/">
                            <h3>Home</h3>
                        </Link>
                        <h2>Welcome, {user.firstname}!</h2>
                        <Link className="header__title" to="/login">
                            <h3>Logout</h3>
                        </Link>
                    </div>
                </div>
            </header>
        )
    }
};

function mapStateToProps(state) {
    return {
        user: state.authentication.user[0]
    };
}

const connectedHeader = connect(mapStateToProps)(Header);
export { connectedHeader as Header }; 
