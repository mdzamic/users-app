import React from 'react'

export const Footer = () => (
    <footer className="footer">
        <div className="content-container">
            <div className="footer__content">
                <div className="footer__title">© {new Date().getFullYear()} Copyright Miodrag Dzamic</div>
            </div>
        </div>
    </footer>
)

