import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import selectUsers from '../_selectors/users.selector';


export const UsersSummary = ({ userCount }) => {
  const userWord = userCount === 1 ? 'user' : 'users';
  return (
    <div className="page-header">
      <div className="content-container">        
        <div className="page-header__actions">
          <h1 className="page-header__title">Viewing <span>{userCount}</span> {userWord}</h1>
          <div className="page-header__actions">
            <Link className="button" to="/create">Add User</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  const visibleUsers = selectUsers(state.users, state.filters);
  return {
    userCount: visibleUsers.length,
  };
};

export default connect(mapStateToProps)(UsersSummary);