import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

//import { registerUser } from '../_actions/user.actions';

class RegisterPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { firstname, lastname, email, password } = this.state;
        const user = { firstname, lastname, email, password }
        const { dispatch } = this.props;
        if (firstname && lastname && email && password) {
            //dispatch(registerUser(user))
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { firstname, lastname, email, password, submitted } = this.state;
        const { message } = this.props;
        return (
            <div>
                <div className="content-container">
                    <h1 className="page-header__title">Register form</h1>
                </div>
                <div className="content-container">
                    {message && <p className="form__error">{message}</p>}
                    <form className="form" name="form" onSubmit={this.handleSubmit}> 
                        <input 
                        type="text" 
                        className="text-input" 
                        placeholder="Firstname" 
                        name="firstname" 
                        value={firstname} 
                        onChange={this.handleChange} 
                        />
                        {submitted && !firstname && <div className="help-block">Firstname is required</div>}

                        <input 
                        type="text" 
                        className="text-input" 
                        placeholder="Lastname" 
                        name="lastname" 
                        value={lastname} 
                        onChange={this.handleChange} 
                        />
                        {submitted && !lastname &&<div className="help-block">Lastname is required</div>}

                        <input 
                        type="email" 
                        className="text-input" 
                        placeholder="Email address" 
                        name="email" 
                        value={email} 
                        onChange={this.handleChange} 
                        />
                        {submitted && !email &&<div className="form__error">Email address is required</div>}
                    
                        <input 
                        type="text" 
                        className="text-input" 
                        placeholder="Password" 
                        name="password" 
                        value={password} 
                        onChange={this.handleChange} 
                        />
                        {submitted && !password &&<div className="form__error">Password is required</div>}
                        
                        <div className="form-group">
                            <button className="button">Register</button>
                            {loggingIn &&
                                <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                            }
                        </div>
                    </form>
                    <Link path to="/login">
                        <button className="button">Back</button>
                    </Link>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        loggingIn: state.authentication.loggingIn,
        message: state.alert.message
    };
}

//const mapDispatchToProps = (dispatch) => ({
//    registerUser: (user) => dispatch(register(user))
//});
  

const connectedRegisterPage = connect(mapStateToProps)(RegisterPage);
export { connectedRegisterPage as RegisterPage }; 