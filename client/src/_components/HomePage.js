import React, { Fragment } from 'react';
import UsersSummary from './UsersSummary';
import UserListFilters from './UserListFilters';
import UserList from './UserList';


export class HomePage extends React.Component {

    render() {
        return (
            <Fragment>
                <UsersSummary />
                <UserListFilters />
                <UserList />
            </Fragment>
        );
    }
}

