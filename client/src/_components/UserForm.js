import React from 'react';

export class UserForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        firstname: props.user ? props.user.firstname : '',
        lastname: props.user ? props.user.lastname : '',
        email: props.user ? props.user.email : '',
        password: props.user ? props.user.password : '',
        error: ''
        }
    }

    onFirstNameChange = (e) => {
        const firstname = e.target.value;
        this.setState(() => ({ firstname }));
    };

    onLastNameChange = (e) => {
        const lastname = e.target.value;
        this.setState(() => ({ lastname }));
    };

    onEmailChange = (e) => {
        const email = e.target.value;
        this.setState(() => ({ email }));
    };

    onPasswordChange = (e) => {
        const password = e.target.value;
        this.setState(() => ({ password }));
    };

    onSubmit = (e) => {
        e.preventDefault();
        if (!this.state.firstname || !this.state.lastname || !this.state.email || !this.state.password) {
        this.setState(() => ({error: 'Please provide Firstname, Lastname, Email and Password'}));
        } else {
        this.setState(() => ({error: ''}));
        this.props.onSubmit({
            firstname: this.state.firstname,
            lastname: this.state.lastname, 
            email: this.state.email,
            password: this.state.password,
        });
        };
    };

    render() {
        return (
            <form className="form" onSubmit={this.onSubmit}>
                {this.state.error && <p className="form__error">{this.state.error}</p>}
                <input 
                type="text"
                placeholder="Firstname"
                autoFocus
                className="text-input"
                value={this.state.firstname}
                onChange={this.onFirstNameChange}
                />
                <input 
                type="text"
                placeholder="Lastname"
                className="text-input"
                value={this.state.lastname}
                onChange={this.onLastNameChange}
                />
                <input 
                type="email"
                placeholder="email"
                className="text-input"
                value={this.state.email}
                onChange={this.onEmailChange}
                />
                <input 
                type="text"
                placeholder="password"
                className="text-input"
                value={this.state.password}
                onChange={this.onPasswordChange}
                />
                <div>
                    <button className="button">Save Item</button>
                </div>
            </form>
        );
    };
} 