import React from 'react';
import { connect } from 'react-redux';
import { setTextFilter, sortByFirstName, sortByLastName, sortByEmail, sortById } from '../_actions/filters.actions';
import { startSetUsers, startGetSingleUser } from '../_actions/user.actions'
import { alertActions } from '../_actions/alert.actions';

export class UserListFilters extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: '',
        };
    }

    handleChange = (e) => {
        this.setState({
            id: e.target.value
        });
    }

    onTextChange = (e) => {
        this.props.setTextFilter(e.target.value);
    };

    onSortChange = (e) => {
        if (e.target.value === 'firstname') {
            this.props.sortByFirstName();
        } else if (e.target.value === 'lastname') {
            this.props.sortByLastName();
        } else if (e.target.value === 'email') {
            this.props.sortByEmail();
        } else if (e.target.value === 'id') {
            this.props.sortById();
        }
    };

    getById = () => {
        const { id } = this.state
        if (id !== '') {
            if (this.props.message) {
                this.props.clearMessage()
            } 
            this.props.startGetSingleUser({id: id });
        }  
    };

    getAll = () => {
        if (this.props.message) {
            this.props.clearMessage()
        } 
        this.props.startSetUsers();
    };

    render() {
        return (
            <div className="content-container">
                <div className="input-group">
                    <div>
                        <div className="input-group__item">
                            <input 
                            type="text"
                            className="text-input"
                            placeholder="Search"
                            value={this.props.filters.text}
                            onChange={this.onTextChange}
                            />
                        </div>
                        <div className="input-group__item">
                            <select 
                            className="select" 
                            value={this.props.filters.sortBy} 
                            onChange={this.onSortChange}
                            >
                            <option value="firstname">Firstname</option>
                            <option value="lastname">Lastname</option>
                            <option value="email">Email</option>
                            <option value="id">ID</option>
                            </select>
                        </div>
                    </div>
                    <div>
                        <input 
                        type="number" 
                        className="text-input input-group__item input-group__item__id" 
                        placeholder="id"  
                        min="1"
                        onChange={this.handleChange}
                        />
                        <button className="button input-group__item" onClick={this.getById}>Get User</button>
                        <button className="button input-group__item" onClick={this.getAll}>Get All Users</button>
                    </div>
                </div>  
                {this.props.message && <p className="form__error">{this.props.message}</p>}      
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        filters: state.filters,
        message: state.alert.message
    };
};

const mapDispatchToProps = (dispatch) => ({
    setTextFilter: (text) => dispatch(setTextFilter(text)),
    sortByFirstName: () => dispatch(sortByFirstName()),
    sortByLastName: () => dispatch(sortByLastName()),
    sortByEmail: () => dispatch(sortByEmail()),
    sortById: () => dispatch(sortById()),
    startSetUsers: () => dispatch(startSetUsers()),
    startGetSingleUser: ({id}) => dispatch(startGetSingleUser({id})),
    clearMessage: () => dispatch(alertActions.clear())

});

export default connect(mapStateToProps, mapDispatchToProps)(UserListFilters);
