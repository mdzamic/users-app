import React from 'react';
import { connect } from 'react-redux';
import { UserForm } from '../_components/UserForm';
import { startEditUser, startRemoveUser } from '../_actions/user.actions';

export class EditUser extends React.Component {
    onSubmit = (user) => {
        this.props.startEditUser(this.props.user.id, user);
        this.props.history.push('/');
    }
    onRemove = () => {
        this.props.startRemoveUser({ id: this.props.user.id });
        this.props.history.push('/');
    };
    render() {
        return (
        <div>
            <div className="page-header">
                <div className="content-container">
                    <div className="page-header__actions">
                        <h1 className="page-header__title">Edit User</h1>
                        <button 
                            className="button button--secondary"
                            onClick={this.onRemove}
                        >Remove User</button>
                    </div>
                </div>
            </div>
            <div className="content-container">
                <UserForm 
                    user={this.props.user}
                    onSubmit={this.onSubmit}
                />
            </div>
        </div>
        );
    };
};

const mapStateToProps = (state, props) => {
    return {
        user: state.users.find((user) => {
            return user.id === parseInt(props.match.params.id)
        })
    };
};

const mapDispatchToProps = (dispatch) => ({
    startEditUser: (id, user) => dispatch(startEditUser(id, user)),
    startRemoveUser: (data) => dispatch(startRemoveUser(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);