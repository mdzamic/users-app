import React from 'react';
import {Link} from 'react-router-dom';

const UserListItem = ({ id, firstname, lastname, email }) => (
    <div>
        <Link className="list-item" to={`/edit/${id}`}>
            <div>
                <h3 className="list-item__title">{firstname} {lastname}</h3>
                <span className="list-item__sub-title">id: {id}</span>
            </div>
            <h3 className="list-item__data">{email}</h3>
        </Link>    
    </div>
);

export default UserListItem;