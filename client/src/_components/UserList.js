import React from 'react';
import {connect} from 'react-redux';
import UserListItem from './UserListItem';
import selectUsers from '../_selectors/users.selector';

export const UserList = (props) => (
  <div className="content-container">
    <div className="list-header">
      <div className="show-for-desktop">Name</div>
      <div className="show-for-desktop">Email</div>
    </div>
    <div className="list-body">
      {
        props.users.length === 0 ? (
          <div className="list-item list-item--message">
            <span>Please change filter, add users or get them from DB!</span>
          </div>
        ) : (
            props.users.map((user) => {
              return <UserListItem key={user.id} {...user}/>
            })
          )
      }
    </div>
  </div>
);

const mapStateToProps = (state) => {
  return {
    users: selectUsers(state.users, state.filters)
  };
};

export default connect(mapStateToProps)(UserList);
