import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from './_helpers/history';
import { alertActions } from './_actions/alert.actions';
import { PrivateRoute } from './_routers/PrivateRoute';
import { LoginPage } from './_components/LoginPage';
import { RegisterPage } from './_components/RegisterPage';
import { HomePage } from './_components/HomePage';
import AddUser from './_components/AddUser';
import EditUser from './_components/EditUser';
import './App.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        
        history.listen(() => {
            props.clearMessage();
        });
    }

    render() {
        return (
            <div className="">
                <div className="">
                    <div className="">
                        <Router history={history}>
                            <Switch>
                                <Route path="/login" component={LoginPage} />
                                {/*<Route path="/register" component={RegisterPage} />*/}
                                <PrivateRoute path="/" component={HomePage} exact={true} />
                                <PrivateRoute path="/create" component={AddUser} />
                                <PrivateRoute path="/edit/:id" component={EditUser} />
                            </Switch>
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
     clearMessage: () => dispatch(alertActions.clear())
});

const connectedApp = connect(undefined,mapDispatchToProps)(App);
export { connectedApp as App }; 