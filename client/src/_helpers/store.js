import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { authentication } from '../_reducers/authentication.reducer';
import { users } from '../_reducers/users.reducer';
import { alert } from '../_reducers/alert.reducer';
import { filters } from '../_reducers/filters.reducer';

const rootReducer = combineReducers({
    authentication, 
    users,
    alert,
    filters
});

const loggerMiddleware = createLogger();

export const store = createStore(
    rootReducer,
    applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    )
);